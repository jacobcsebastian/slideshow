import React, { Component } from 'react';
import Slideshow from './Slideshow/Slideshow';
import './App.css';

class App extends Component {
  state = {
  }

  render() {
    console.log(this.state.pastedImage);
    return (
      <div className="App">
        <Slideshow />
      </div>
    );
  }
}

export default App;
