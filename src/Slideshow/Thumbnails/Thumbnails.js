import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import cx from 'classnames';
import './Thumbnails.css';

export default class Thumbnails extends Component {
  static propTypes = {
    imageList: PropTypes.array,
    selectedImage: PropTypes.object,
    onSelection: PropTypes.func,
    onTermination: PropTypes.func,
  };

  componentWillReceiveProps(nextProps) {
    if (this.props.selectedItem !== nextProps.selectedItem) {
      this.setState({ activeImage: nextProps.selectedItem });
    }
  }

  handleClick = (image) => {
    this.props.onSelection(image);
  }

  handleTermination = (image) => {
    this.props.onTermination(image);
  }

  render() {
    const { imageList } = this.props;

    return (
      <div className="thumbnails-container" ref={(container) => { this.container = container; }}>
        {
          _.map(imageList, image => {
            const isSelected = this.props.selectedImage && image.url === this.props.selectedImage.url;
            const classname = cx('thumbnail-button', {
              'thumbnail-button-selected': isSelected,
              'thumbnail-button-inactive': !image.active,
              'thumbnail-button-active': image.active,
            });

            return (
              <button
                key={image.url}
                className={classname}
                ref={(imageRef) => {
                  if (isSelected) {
                    this.imageRef = imageRef;
                  }
                }}
                onClick={() => this.handleClick(image)}
                onDoubleClick={() => this.handleTermination(image)}
              >
                <img className="thumbnail-image" src={image.url} alt="thumbnail" />
              </button>
            );
          })
        }
      </div>
    )
  }
}
