import React, { Component } from 'react';
import _ from 'lodash';
import SelectedSlide from './SelectedSlide/SelectedSlide';
import Thumbnails from './Thumbnails/Thumbnails';
import { retrieveImageFromClipboardAsBlob, getActiveImageList } from './utils';
import './Slideshow.css';

export default class Slideshow extends Component {
  state = {
    imageList: [],
    activeSlide: undefined,
    activeSlideIndex: 0,
    slideShowPaused: false,
  }

  timer = undefined;

  componentDidMount() {
    const that = this;
    window.addEventListener("paste", function (e) {
      retrieveImageFromClipboardAsBlob(e, that.handleImagePasting);
    }, false);
  }

  handleImagePasting = (imageBlob) => {
    if (!imageBlob) {
      return;
    }
    const newImageUrl = URL.createObjectURL(imageBlob);
    const imageObject = { url: newImageUrl, active: true };

    window.clearInterval(this.timer);

    this.setState((prevState) => {
      const imageList = [imageObject, ...prevState.imageList];
      return { imageList, activeSlide: imageObject, activeSlideIndex: 0, }
    }, () => { this.startSlideshow(); });
  }

  handleThumbnailClick = (selectedImage) => {
    const { imageList, activeSlide } = this.state;
    const newImageList = _.map(imageList, image => {
      if (image.url === selectedImage.url && activeSlide.url !== selectedImage.url) {
        image.active = !selectedImage.active;
      }
      return image;
    });

    this.setState({ imageList: newImageList, activeSlideIndex: -1, });
  }

  handleOnTermination = (imageToRemove) => {
    window.clearInterval(this.timer);
    this.setState(prevState => {
      const listOfImages = [...prevState.imageList];
      _.remove(listOfImages, item => {
        return item.url === imageToRemove.url;
      });
      const activeImageList = getActiveImageList(listOfImages);

      return ({
        ...prevState,
        imageList: listOfImages,
        activeSlide: activeImageList.length === 0 ? undefined : prevState.activeSlide,
      });
    }, () => { this.startSlideshow(); });
  }

  handleClickMainSlide = () => {
    this.setState((state) => ({ slideShowPaused: !state.slideShowPaused }), () => {
      this.state.slideShowPaused ? this.pauseSlideShow() : this.startSlideshow();
    });
  }

  startSlideshow = () => {
    this.timer = window.setInterval(() => {
      const { imageList, activeSlideIndex } = this.state;
      const activeImageList = getActiveImageList(imageList);


      if (activeImageList.length <= 1) {
        this.setState({
          activeSlide: activeImageList.length === 1 ? activeImageList[0] : undefined,
        });
        return;
      }

      let nextIndex = activeSlideIndex + 1;

      if (nextIndex >= activeImageList.length) {
        nextIndex = 0;
      }

      this.setState({
        activeSlideIndex: nextIndex,
        activeSlide: activeImageList[nextIndex]
      });
    }, 2000);
  }

  pauseSlideShow = () => {
    window.clearInterval(this.timer);
  }

  render() {
    const { imageList, activeSlide, slideShowPaused } = this.state;

    return (
      <div className="slideshow-container">
        {
          imageList.length === 0
            ? (<div className="slideshow-empty-text">No images available</div>)
            : null
        }
        <div className="selectedslide-container">
          {
            _.get(activeSlide, 'url')
              ? (
                <SelectedSlide
                  selectedImage={activeSlide.url}
                  onClick={this.handleClickMainSlide}
                  isPaused={slideShowPaused}
                />
              )
              : null
          }
        </div>

        {
          imageList.length > 0
            ?
            <Thumbnails
              imageList={imageList}
              selectedImage={activeSlide}
              onSelection={this.handleThumbnailClick}
              onTermination={this.handleOnTermination}
            />
            : null
        }
      </div>
    )
  }
}
