import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import './SelectedSlide.css';


export default class SelectedSlide extends Component {
  static propTypes = {
    selectedImage: PropTypes.string,
    isPaused: PropTypes.bool,
    onClick: PropTypes.func,
  };

  state = {
    imageToFadeOut: false,
    imageToDisplay: undefined,
  }
  componentWillMount() {
    const { selectedImage } = this.props;
    this.setState({ imageToDisplay: selectedImage })
  }

  componentWillReceiveProps(nextProps) {
    const { imageToDisplay } = this.state;

    if (imageToDisplay !== nextProps.selectedImage) {
      this.setState({ imageToFadeOut: true });
      window.setTimeout(() => {
        this.setState({ imageToFadeOut: false, imageToDisplay: nextProps.selectedImage });
      }, 100)
    }
  }


  render() {
    const { imageToDisplay, imageToFadeOut } = this.state;
    const { isPaused, onClick } = this.props;

    const imageClassName = cx('selectedslide-active-image', {
      'selectedslide-image-fadeout': imageToFadeOut,
      'selectedslide-image-fadein': !imageToFadeOut,
      'selectedslide-image-paused': isPaused,
    });
    
    const buttonClassName = cx('selectedslide-button', {
      'selectedslide-button-paused': isPaused,
    });
    if (!imageToDisplay) {
      return null;
    }


    return (
      <button className={buttonClassName} onClick={onClick} >
        <img src={imageToDisplay} className={imageClassName} alt="slideshow"/>
        {
          isPaused
            ? <div className="selectedslide-pause-button" />
            : null
        }
      </button>
    );
  }
}
